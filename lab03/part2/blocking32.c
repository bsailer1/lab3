#include <stdio.h>
#include <math.h>

#define XSIZE 100
#define YSIZE 100

int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];

  int i, j, k;
  
  int r;

  /* Initialize x matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j + 2;
    }
  }

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
			z[i][j] = i + j + 2;
    }
  }

	long int count = 0;
	long int iter = 0;
	unsigned int tag_shift = 4294934528; // This is 1's in the 17 MSBs
	unsigned int index_shift = 32736; // This is 1's in the middle 10 bits
	unsigned int offset_shift = 31; // This is 1's in the 5 LSBs
	
	int valid[1024];
	int tag[1024];
	int a, temp, jj, kk;

	for(a=0; a < 1024; a++){
		valid[a] = 0;
		tag[a] = 0;
	}

  /* Do matrix multiply */

	int N = XSIZE; // Matrix size
	int B = 38;// Blocking factor
  /* Do matrix multiply */
	for(jj=0;jj<N;jj+= B){
		for(kk=0;kk<N;kk+= B){
			for(i=0;i<N;i++){
				for(j = jj; j<((jj+B)>N?N:(jj+B)); j++){
					temp = 0;
					for(k = kk; k<((kk+B)>N?N:(kk+B)); k++){
						temp += y[i][k]*z[k][j];
						iter += 2;
			
						int* y_temp = &y[i][k];
						int* z_temp = &z[k][j];
						unsigned int ytemp = (unsigned) y_temp;
						unsigned int ztemp = (unsigned) z_temp;
						unsigned int y_tag = (ytemp & tag_shift) >> 15;
						unsigned int z_tag = (ztemp & tag_shift) >> 15;
						unsigned int y_index = (ytemp & index_shift) >> 5;
						unsigned int z_index = (ztemp & index_shift) >> 5;
						unsigned int y_offset = ytemp & offset_shift;
						unsigned int z_offset = ztemp & offset_shift;

						if(valid[y_index] == 0){
							valid[y_index] = 1;
							tag[y_index] = y_tag;
							count += 1;
						} else if (valid[y_index] == 1 && tag[y_index] != y_tag){
							tag[y_index] = y_tag;
							count += 1;
						}
				
						if(valid[z_index] == 0){
							valid[z_index] = 1;
							tag[z_index] = z_tag;
							count += 1;
						} else if (valid[z_index] == 1 && tag[z_index] != z_tag){
								tag[z_index] = z_tag;
								count += 1;
						}
					}
					x[i][j] += temp;
					iter += 1;

					int* x_temp = &x[i][k];
					unsigned int xtemp = (unsigned) x_temp;
					unsigned int x_tag = (xtemp & tag_shift) >> 15;
					unsigned int x_index = (xtemp & index_shift) >> 5;
					unsigned int x_offset = xtemp & offset_shift;

					if(valid[x_index] == 0){
						valid[x_index] = 1;
						tag[x_index] = x_tag;
						count += 1;
					} else if (valid[x_index] == 1 && tag[x_index] != x_tag){
						tag[x_index] = x_tag;
						count += 1;
					}
				}
			}
		}
	}
	/*
	for(i=0; i < XSIZE; i++){
		for(j=0; j < XSIZE; j++){
			printf("%d ", x[i][j]);
		}
		printf("\n");
	}
	*/
	printf("Misses: %d\n", count);
	printf("Iter: %d\n", iter);
}


