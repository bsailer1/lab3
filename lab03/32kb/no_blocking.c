#include <stdio.h>
#include <math.h>

#define XSIZE 8
#define YSIZE 8

int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];

  int i, j, k;
  
  int r;

  /* Initialize x matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
    }
  }

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
			z[i][j] = i + j;
    }
  }
	
	for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      printf("%d ", y[i][j]);
    }
		printf("\n");
  }
	printf("\n");

	long int count = 0;
	unsigned int tag_shift = 4294934528; // This is 1's in the 17 MSBs
	unsigned int index_shift = 32736; // This is 1's in the middle 10 bits
	unsigned int offset_shift = 31; // This is 1's in the 5 LSBs
	
	int valid[1024];
	int tag[1024];
	int a;

	for(a=0; a < 1024; a++){
		valid[a] = 0;
		tag[a] = 0;
	}

  /* Do matrix multiply */
  for(i=0; i<XSIZE; i=i+1) {
    for(j=0; j<YSIZE; j=j+1) {
      r = 0;
      for(k=0; k<XSIZE; k=k+1) {
        r = r + y[i][k] * z[k][j];

				int* y_temp = &y[i][k];
				int* z_temp = &z[k][j];
				unsigned int ytemp = (unsigned int) y_temp;
				unsigned int ztemp = (unsigned int) z_temp;

				// This isolates the far 17 MSBs
				unsigned int y_tag = (ytemp & tag_shift) >> 15;
				unsigned int z_tag = (ztemp & tag_shift) >> 15;

				// This isolates the index by shifting over 5 (the size of the offset)
				unsigned int y_index = (ytemp & index_shift) >> 5;
				unsigned int z_index = (ztemp & index_shift) >> 5;
				
				unsigned int y_offset = ytemp & offset_shift;
				unsigned int z_offset = ztemp & offset_shift;

				if(valid[y_index] == 0){
					valid[y_index] = 1;
					tag[y_index] = y_tag;
					count += 1;
				} else if (valid[y_index] == 1 && tag[y_index] != y_tag){
					tag[y_index] = y_tag;
					count += 1;
				}
				
				if(valid[z_index] == 0){
					valid[z_index] = 1;
					tag[z_index] = z_tag;
					count += 1;
				} else if (valid[z_index] == 1 && tag[z_index] != z_tag){
					tag[z_index] = z_tag;
					count += 1;
				}
	
      }
      x[i][j] = r;
    }
  }
  
	for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      printf("%d ", x[i][j]);
    }
		printf("\n");
  }

	printf("Misses: %d\n", count);

}
