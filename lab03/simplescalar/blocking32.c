#include <stdio.h>
#include <math.h>

#define XSIZE 100
#define YSIZE 100

int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];

  int i, j, k;
  
  int r;

  /* Initialize x matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j + 2;
    }
  }

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
			z[i][j] = i + j + 2;
    }
  }

	long int count = 0;
	long int iter = 0;
	int a, temp, jj, kk;

  /* Do matrix multiply */

	int N = XSIZE; // Matrix size
	int B = 1;// Blocking factor
  /* Do matrix multiply */
	for(jj=0;jj<N;jj+= B){
		for(kk=0;kk<N;kk+= B){
			for(i=0;i<N;i++){
				for(j = jj; j<((jj+B)>N?N:(jj+B)); j++){
					temp = 0;
					for(k = kk; k<((kk+B)>N?N:(kk+B)); k++){
						temp += y[i][k]*z[k][j];
					}
					x[i][j] += temp;
				}
			}
		}
	}
}


