#include <stdio.h>
#include <math.h>

#define XSIZE 100
#define YSIZE 100

int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];

  int i, j, k;
  
  int r;

  /* Initialize x matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
    }
  }

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
	z[i][j] = i + j;
    }
  }

	long int count = 0;
	unsigned int tag_shift = 4294836224; // This is 1's in the first 15 LSB
	unsigned int index_shift = 131008; // This is 1's in between the tag and the offset (middle 11 bits)
	unsigned int offset_shift = 63; // This is 1's in the first 6 LSB
	
	int valid[2048];
	int tag[2048];
	int a;

	for(a=0; a < 2048; a++){
		valid[a] = 0;
		tag[a] = 0;
	}

  /* Do matrix multiply */
  for(i=0; i<XSIZE; i=i+1) {
    for(j=0; j<YSIZE; j=j+1) {
      r = 0;
      for(k=0; k<XSIZE; k=k+1) {
        r = r + y[i][k] * z[k][j];

				int y_temp = &y[i][k];
				int z_temp = &z[k][j];
				unsigned int ytemp = (unsigned) y_temp;
				unsigned int ztemp = (unsigned) z_temp;

				unsigned int ytag = ytemp & tag_shift;
				unsigned int ztag = ztemp & tag_shift;
				unsigned int y_tag = ytag >> 17; // Shift by 17 to isolate the 15 MSBs
				unsigned int z_tag = ztag >> 17;

				unsigned int yindex = ytemp & index_shift;
				unsigned int zindex = ztemp & index_shift;
				unsigned int y_index = yindex >> 6; // Shift by 6 to isolate the 6 MSBs (that aren't 0's)
				unsigned int z_index = zindex >> 6;
				
				unsigned int y_offset = ytemp & offset_shift;
				unsigned int z_offset = ztemp & offset_shift;

				if(valid[y_index] == 0){
					valid[y_index] = 1;
					tag[y_index] = y_tag;
					count += 1;
				} else if (valid[y_index] == 1 && tag[y_index] != y_tag){
					tag[y_index] = y_tag;
					count += 1;
				}
				
				if(valid[z_index] == 0){
					valid[z_index] = 1;
					tag[z_index] = z_tag;
					count += 1;
				} else if (valid[z_index] == 1 && tag[z_index] != z_tag){
					tag[z_index] = z_tag;
					count += 1;
				}
	
      }
      x[i][j] = r;
    }
  }

	printf("Misses: %d\n", count);

	/*
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
			int temp = &x[i][j];
			unsigned int temp2 = (unsigned) temp;
			printf("%x ", temp2);
    }
		printf("\n");
  }
	*/
}
